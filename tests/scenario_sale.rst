=============
Sale Scenario
=============

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install sale::

    >>> config = activate_modules('sale_per_country_report')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product.template = template
    >>> product.save()

    >>> service = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> service.template = template
    >>> service.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Sale 5 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.type = 'comment'
    >>> sale_line.description = 'Comment'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 3.0
    >>> sale.click('quote')
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('50.00'), Decimal('5.00'), Decimal('55.00'))
    >>> sale.click('confirm')
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('50.00'), Decimal('5.00'), Decimal('55.00'))
    >>> sale.click('process')
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('50.00'), Decimal('5.00'), Decimal('55.00'))
    >>> sale.state
    'processing'
    >>> len(sale.shipments), len(sale.shipment_returns), len(sale.invoices)
    (1, 0, 1)
    >>> invoice, = sale.invoices
    >>> invoice.origins == sale.rec_name
    True
    >>> shipment, = sale.shipments
    >>> shipment.origins == sale.rec_name
    True

Check report with shipment address::

    >>> per_country = Wizard('sale.sale_per_country.print', [])
    >>> per_country.form.start_date = today
    >>> per_country.form.end_date = today
    >>> per_country.form.type_ = 'all'
    >>> per_country.form.quotation = True
    >>> per_country.form.category = category
    >>> per_country.form.restrict_countries = False
    >>> per_country.form.invoice_address
    False
    >>> per_country.form.grouping
    'product'
    >>> per_country.execute('print_')
    >>> (_, _, _, _), = per_country.actions

    >>> per_country = Wizard('sale.sale_per_country.print', [])
    >>> per_country.form.start_date = today
    >>> per_country.form.end_date = today
    >>> per_country.form.type_ = 'all'
    >>> per_country.form.quotation = True
    >>> per_country.form.category = category
    >>> per_country.form.restrict_countries = False
    >>> per_country.form.grouping = 'country'
    >>> per_country.execute('print_')
    >>> (_, _, _, _), = per_country.actions

Check report with invoice address::

    >>> per_country = Wizard('sale.sale_per_country.print', [])
    >>> per_country.form.start_date = today
    >>> per_country.form.end_date = today
    >>> per_country.form.type_ = 'all'
    >>> per_country.form.quotation = True
    >>> per_country.form.category = category
    >>> per_country.form.restrict_countries = False
    >>> per_country.form.invoice_address = True
    >>> per_country.execute('print_')
    >>> (_, _, _, _), = per_country.actions

    >>> per_country = Wizard('sale.sale_per_country.print', [])
    >>> per_country.form.start_date = today
    >>> per_country.form.end_date = today
    >>> per_country.form.type_ = 'all'
    >>> per_country.form.quotation = True
    >>> per_country.form.category = category
    >>> per_country.form.restrict_countries = False
    >>> per_country.form.grouping = 'country'
    >>> per_country.form.invoice_address = True
    >>> per_country.execute('print_')
    >>> (_, _, _, _), = per_country.actions

Configure default countries::

    >>> Configuration = Model.get('sale.configuration')
    >>> conf = Configuration(1)
    >>> conf.report_country_filter = '61;113'
    >>> conf.save()
    >>> per_country = Wizard('sale.sale_per_country.print', [])
    >>> per_country.form.start_date = today
    >>> per_country.form.end_date = today
    >>> per_country.form.type_ = 'export'
    >>> per_country.form.category = None
    >>> per_country.form.quotation = False
    >>> per_country.form.restrict_countries
    True
    >>> len(per_country.form.countries)
    2
    >>> per_country.execute('print_')
